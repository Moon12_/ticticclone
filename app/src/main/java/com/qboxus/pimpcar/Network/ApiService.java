package com.qboxus.pimpcar.Network;


import com.google.gson.JsonObject;
import com.qboxus.pimpcar.pojos.StickerApiResponse;
import com.qboxus.pimpcar.pojos.WheelApiResponse;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;


public interface ApiService {
    @POST(NetworkConstants.WHEEL_API_API_END)
    Single<WheelApiResponse> getWheel(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String user_jwt_key);


    @POST(NetworkConstants.STICKER_API_END)
    Single<StickerApiResponse> getStickerWheel(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String user_jwt_key);
}

package com.qboxus.pimpcar.retrofitServices;

import com.google.gson.JsonObject;
import com.qboxus.pimpcar.ApiClasses.ApiLinks;
import com.qboxus.pimpcar.Constants;
import com.qboxus.pimpcar.retrofitServices.models.GetAllPicResponseModel;
import com.qboxus.pimpcar.retrofitServices.models.PostLikeResponseModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface GetDateService {

    @FormUrlEncoded
    @POST("all-pictures")
    Call<GetAllPicResponseModel> getAllPics (
            @Field("user_id") String  userid

    );

    @Headers({
            "Content-Type: application/json",
    })
    @POST(ApiLinks.likeImagePost)
    Call<PostLikeResponseModel> likeDislikeApi(@Body JsonObject params);


}

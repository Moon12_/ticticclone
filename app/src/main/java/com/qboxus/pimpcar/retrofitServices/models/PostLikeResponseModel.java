package com.qboxus.pimpcar.retrofitServices.models;

import com.google.gson.annotations.SerializedName;

public class PostLikeResponseModel {
    @SerializedName("like")
    private String message;
   @SerializedName("unlike")
    private String unlike;

    public String getUnlike() {
        return unlike;
    }

    public void setUnlike(String unlike) {
        this.unlike = unlike;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

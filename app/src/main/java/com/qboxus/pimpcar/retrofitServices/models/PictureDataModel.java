package com.qboxus.pimpcar.retrofitServices.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PictureDataModel {

    @SerializedName("id")
    private int orderId;
    @SerializedName("title")
    private String title;
     @SerializedName("body")
     private String body;
    @SerializedName("user_id")
    private String user_id;
    @SerializedName("type")
    private String type;
    @SerializedName("file_path")
    private String file_path;
    @SerializedName("status")
    private String status;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("likes_count")
    private String likes_count;
    @SerializedName("comments_count")
    private String comments_count;
    @SerializedName("user")
    private PicsUserModel user;
    @SerializedName("comments")
    private ArrayList<PictureCommentsModel> comments;
    public PicsUserModel getUser() {
        return user;
    }

    public void setUser(PicsUserModel user) {
        this.user = user;
    }

    public int getPostId() {
        return orderId;
    }

    public ArrayList<PictureCommentsModel> getComments() {
        return comments;
    }

    public void setComments(ArrayList<PictureCommentsModel> comments) {
        this.comments = comments;
    }

    public void setPostId(int orderId) {
        this.orderId = orderId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getLikes_count() {
        return likes_count;
    }

    public void setLikes_count(String likes_count) {
        this.likes_count = likes_count;
    }

    public String getComments_count() {
        return comments_count;
    }

    public void setComments_count(String comments_count) {
        this.comments_count = comments_count;
    }
}

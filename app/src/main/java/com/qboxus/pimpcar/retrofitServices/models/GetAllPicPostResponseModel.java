package com.qboxus.pimpcar.retrofitServices.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GetAllPicPostResponseModel {

    @SerializedName("current_page")
    private int current_page;
    @SerializedName("data")
    private ArrayList<PictureDataModel> data;

    public int getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(int current_page) {
        this.current_page = current_page;
    }

    public ArrayList<PictureDataModel> getData() {
        return data;
    }

    public void setData(ArrayList<PictureDataModel> data) {
        this.data = data;
    }
}

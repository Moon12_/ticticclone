package com.qboxus.pimpcar.retrofitServices.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PicsUserModel {

    @SerializedName("id")
    private int id;
      @SerializedName("name")
    private String name;
      @SerializedName("username")
    private String username;
      @SerializedName("phone")
    private String phone;
      @SerializedName("bio")
    private String bio;
      @SerializedName("role")
    private String role;
      @SerializedName("banned")
    private String banned;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getBanned() {
        return banned;
    }

    public void setBanned(String banned) {
        this.banned = banned;
    }
}

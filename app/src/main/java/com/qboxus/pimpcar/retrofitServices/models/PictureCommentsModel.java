package com.qboxus.pimpcar.retrofitServices.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PictureCommentsModel {

    @SerializedName("id")
    private int orderId;
    @SerializedName("post_id")
    private String post_id;

    @SerializedName("user_id")
    private String user_id;
    @SerializedName("body")
    private String body;
    @SerializedName("user")
    private CommentUserModel user;

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public CommentUserModel getUser() {
        return user;
    }

    public void setUser(CommentUserModel user) {
        this.user = user;
    }
}

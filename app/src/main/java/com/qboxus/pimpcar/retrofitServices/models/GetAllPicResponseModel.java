package com.qboxus.pimpcar.retrofitServices.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetAllPicResponseModel {


    @SerializedName("posts")
    private GetAllPicPostResponseModel posts;

    public GetAllPicPostResponseModel getPosts() {
        return posts;
    }

    public void setPosts(GetAllPicPostResponseModel posts) {
        this.posts = posts;
    }
}

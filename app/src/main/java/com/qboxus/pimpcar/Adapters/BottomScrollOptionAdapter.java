package com.qboxus.pimpcar.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;


import com.qboxus.pimpcar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BottomScrollOptionAdapter extends RecyclerView.Adapter<BottomScrollOptionAdapter.BottomViewHolder> {

    private Context _mContext;
    private int[] category_icons;
    private String[] category_name;
    private LayoutInflater layoutInflater;
    private BottomViewHolder bottomViewHolder;

    public BottomScrollOptionAdapter(Context context, int[] bottom_icons, String[] category) {
        this._mContext = context;
        this.category_icons = bottom_icons;
        this.category_name = category;
        this.layoutInflater = LayoutInflater.from(_mContext);
    }

    @NonNull
    @Override
    public BottomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = layoutInflater.inflate(R.layout.bottom_category_selection_view, parent, false);
        ButterKnife.bind(this, itemView);
        bottomViewHolder = new BottomViewHolder(itemView);
        return bottomViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull BottomViewHolder bottomViewHolder, int position) {

        bottomViewHolder.text_title.setText(category_name[position] + "");
        bottomViewHolder.image_icon.setImageResource(category_icons[position]);

    }

    @Override
    public int getItemCount() {
        return category_icons.length;
    }

    public class BottomViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_icon)
        AppCompatImageView image_icon;

        @BindView(R.id.text_title)
        AppCompatTextView text_title;

        public BottomViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

package com.qboxus.pimpcar.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;
import com.qboxus.pimpcar.ActivitesFragment.imagesPart.dialog.ActionBottomDialogFragment;
import com.qboxus.pimpcar.Constants;
import com.qboxus.pimpcar.R;
import com.qboxus.pimpcar.retrofitServices.GetDateService;
import com.qboxus.pimpcar.retrofitServices.models.PictureDataModel;
import com.qboxus.pimpcar.retrofitServices.models.PostLikeResponseModel;
import com.qboxus.pimpcar.utils.Utilities;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ImagePostAdapter extends RecyclerView.Adapter<ImagePostAdapter.ViewHolder> {
    private List<PictureDataModel> listdata;
    private Context ctx;
    Callback callback;

    // RecyclerView recyclerView;
    public ImagePostAdapter(List<PictureDataModel> listdata, Context context, Callback callback) {
        this.listdata = listdata;
        this.ctx = context;
        this.callback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.profile_rv_single_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final PictureDataModel model = listdata.get(position);
        holder.initClickListener();
        Picasso.get().load(model.getFile_path()).into(holder.imageView);
        holder.userName.setText(model.getUser().getUsername());
        holder.post_description.setText(model.getBody());
        holder.post_number_of_likes.setText(model.getLikes_count());
        holder.user_comments_see.setText(model.getComments_count());
        holder.post_like_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postLikeApi(model.getUser_id(), model.getPostId(), holder.post_number_of_likes, holder.post_like_button);
            }
        });
        holder.comment_made_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActionBottomDialogFragment addPhotoBottomDialogFragment2 = ActionBottomDialogFragment.newInstance();
                addPhotoBottomDialogFragment2.setStyle(ActionBottomDialogFragment.STYLE_NORMAL, R.style
                        .CustomBottomSheetDialogTheme);
                String totalComments = model.getComments_count();
                Integer getPostid = model.getPostId();
                Bundle data = new Bundle();//create bundle instance
                data.putInt("position", position);
                Utilities.setPictureComments(ctx, listdata.get(position).getComments());
                data.putString("postId", String.valueOf(getPostid));//put string to pass with a key value
                data.putString("totalComments", totalComments);//put string to pass with a key value
                addPhotoBottomDialogFragment2.setArguments(data);//Set bundle data to fragment
                /*for fragment*/

                addPhotoBottomDialogFragment2.show(((FragmentActivity) ctx).getSupportFragmentManager(),

                        ActionBottomDialogFragment.TAG);


            }
        });

    }


    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public interface Callback {
        void onItemClick(int pos);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public AppCompatImageView imageView, AppCompatImageView, post_like_button;
        public TextView userName;
        public AppCompatTextView post_description, post_number_of_likes, user_comments_see;

        public RelativeLayout relativeLayout, comment_made_rl;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imageView = itemView.findViewById(R.id.user_post_image);
            this.user_comments_see = itemView.findViewById(R.id.user_comments_see);
            this.post_number_of_likes = itemView.findViewById(R.id.post_number_of_likes);
            this.userName = (TextView) itemView.findViewById(R.id.user_name_tv);
            this.post_description = itemView.findViewById(R.id.post_description);
            this.post_like_button = itemView.findViewById(R.id.post_like_button);
            this.comment_made_rl = itemView.findViewById(R.id.comment_made_rl);
//            relativeLayout = (RelativeLayout)itemView.findViewById(R.id.rlTopp);
        }


        private void initClickListener() {
            comment_made_rl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.onItemClick(getAdapterPosition());
                }
            });
        }
    }

    private void postLikeApi(String userid, Integer post_id, AppCompatTextView totalLikes, AppCompatImageView postLikeBtn) {
//        KProgressHUD hud = KProgressHUD.create(ctx)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel("")
//                .setMaxProgress(100)
//                .show();
//        hud.setProgress(90);
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("user_id", userid);
        jsonObject.addProperty("post_id", post_id);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Create a file object using file path

        GetDateService getResponse = retrofit.create(GetDateService.class);
//        Call<String> call = getResponse.uploadImage(fileToUpload,"descriptions","hashtags","userTags","6184be3e6274da6a8865e3ed","1637392740");
        Call<PostLikeResponseModel> call = getResponse.likeDislikeApi(jsonObject);
        call.enqueue(new retrofit2.Callback<PostLikeResponseModel>() {
            @Override
            public void onResponse(Call<PostLikeResponseModel> call, Response<PostLikeResponseModel> response) {
                if (response.code() == 200) {
//                    hud.dismiss();
                    if (response.body().getMessage().equals("Like Successfully")) {
                        int totallikesIncremnt = Integer.parseInt(totalLikes.getText().toString()) + 1;
                        totalLikes.setText(String.valueOf(totallikesIncremnt));
                        postLikeBtn.setImageResource(R.drawable.ic_baseline_favorite_24);

                        Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        if (totalLikes.getText().toString().equals("1")) {
                            totalLikes.setText("0");
//                            Toast.makeText(ctx, "call", Toast.LENGTH_SHORT).show();
                            postLikeBtn.setImageResource(R.drawable.white_ic_baseline_favorite_24);
                            Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        } else {
                            int totallikesIncremnt = Integer.parseInt(totalLikes.getText().toString()) - 1;
                            totalLikes.setText(String.valueOf(totallikesIncremnt));
                            postLikeBtn.setImageResource(R.drawable.white_ic_baseline_favorite_24);
                            Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        }


                    }

                } else {
//                    hud.dismiss();
                    Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
//                hud.dismiss();
                Log.d("gttt", call.toString());
                Toast.makeText(ctx, "failed", Toast.LENGTH_SHORT).show();

            }
        });

    }
}
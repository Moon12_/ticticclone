package com.qboxus.pimpcar.ActivitesFragment.imagesPart.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.material.tabs.TabLayout;
import com.qboxus.pimpcar.ActivitesFragment.Profile.ProfileA;
import com.qboxus.pimpcar.ActivitesFragment.imagesPart.ImagesCommentF;
import com.qboxus.pimpcar.Adapters.ImagePostAdapter;
import com.qboxus.pimpcar.Constants;
import com.qboxus.pimpcar.MainMenu.MainMenuFragment;
import com.qboxus.pimpcar.MainMenu.RelateToFragmentOnBack.RootFragment;
import com.qboxus.pimpcar.Models.NotificationModel;
import com.qboxus.pimpcar.R;
import com.qboxus.pimpcar.SimpleClasses.Functions;
import com.qboxus.pimpcar.SimpleClasses.Variables;
import com.qboxus.pimpcar.retrofitServices.GetDateService;
import com.qboxus.pimpcar.retrofitServices.RetrofitClientInstance;
import com.qboxus.pimpcar.retrofitServices.models.GetAllPicResponseModel;
import com.qboxus.pimpcar.retrofitServices.models.PictureDataModel;
import com.qboxus.pimpcar.utils.Utilities;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ImageAllPostFragment extends RootFragment implements View.OnClickListener, ImagePostAdapter.Callback {

    View view;
    Context context;

    RecyclerView recyclerView;
    private ArrayList<PictureDataModel> pictureDataModels;
    SwipeRefreshLayout swiperefresh;
    private ImagePostAdapter adapter;
    LinearLayout dataContainer;
    ShimmerFrameLayout shimmerFrameLayout;

    int pageCount = 0;
    boolean ispostFinsh;

    ProgressBar loadMoreProgress;
    LinearLayoutManager linearLayoutManager;

    boolean isApiCall = false;

    public ImageAllPostFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_image_post, container, false);
        context = getContext();


        pictureDataModels = new ArrayList<>();

        shimmerFrameLayout = view.findViewById(R.id.shimmer_view_container);
        dataContainer = view.findViewById(R.id.dataContainer);
        recyclerView = (RecyclerView) view.findViewById(R.id.recylerview);

        getAllPicsApi();
        view.findViewById(R.id.inbox_btn).setOnClickListener(this);
        swiperefresh = view.findViewById(R.id.swiperefresh);
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (pictureDataModels.size() < 1) {
                    dataContainer.setVisibility(View.GONE);
                    shimmerFrameLayout.setVisibility(View.VISIBLE);
                    shimmerFrameLayout.startShimmer();
                }
                pageCount = 0;
                getAllPicsApi();
            }
        });


        return view;
    }


    // load the banner and show on below of the screen
    AdView adView;

    @Override
    public void onStart() {
        super.onStart();
        adView = view.findViewById(R.id.bannerad);
        if (!Constants.IS_REMOVE_ADS) {
            AdRequest adRequest = new AdRequest.Builder().build();
            adView.loadAd(adRequest);
        } else {
            adView.setVisibility(View.GONE);
        }

    }


    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
//        if (((pageCount == 0 && visible)) || Variables.reloadMyNotification) {
//            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    Variables.reloadMyNotification = false;
//
//                    if (pictureDataModels.size() < 1) {
//                        dataContainer.setVisibility(View.GONE);
//                        shimmerFrameLayout.setVisibility(View.VISIBLE);
//                        shimmerFrameLayout.startShimmer();
//                    }
//
//                    pageCount = 0;
//                    getAllPicsApi();
//                }
//            }, 200);
//        }
        getAllPicsApi();
    }


    // get the all notification from the server against the profile id

    private void getAllPicsApi() {

        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
        Call<GetAllPicResponseModel> call = service.getAllPics("1");
        call.enqueue(new Callback<GetAllPicResponseModel>() {
            @Override
            public void onResponse(Call<GetAllPicResponseModel> call, Response<GetAllPicResponseModel> response) {
                isApiCall = false;
                shimmerFrameLayout.stopShimmer();
                shimmerFrameLayout.setVisibility(View.GONE);
                dataContainer.setVisibility(View.VISIBLE);
                swiperefresh.setRefreshing(false);
                pictureDataModels = response.body().getPosts().getData();
                if (pictureDataModels != null && !pictureDataModels.isEmpty()) {
                    Toast.makeText(getActivity(), "success", Toast.LENGTH_SHORT).show();
                    showcurrentOrders(pictureDataModels, recyclerView);

                } else {
                    Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetAllPicResponseModel> call, Throwable t) {
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });

    }

    private void showcurrentOrders(List<PictureDataModel> list, RecyclerView recyclerView) {
        recyclerView.setHasFixedSize(true);
        adapter = new ImagePostAdapter(list, getActivity(), ImageAllPostFragment.this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
    }

    // parse the data of the notification and place then on data model list


    // open the broad cast live user streaming on notification receive


    // open the profile of the user which notification we have receive
    public void openProfile(NotificationModel item) {
        if (Functions.getSharedPreference(context).getString(Variables.U_ID, "0").equals(item.user_id)) {

            TabLayout.Tab profile = MainMenuFragment.tabLayout.getTabAt(4);
            profile.select();

        } else {

            Intent intent = new Intent(view.getContext(), ProfileA.class);
            intent.putExtra("user_id", item.user_id);
            intent.putExtra("user_name", item.username);
            intent.putExtra("user_pic", item.profile_pic);
            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);

        }

    }


    @Override
    public void onClick(View v) {

    }

    @Override
    public void onItemClick(int pos) {
//        openComment(pictureDataModels.get(pos).getComments_count(), pos);

    }

    private void openComment(String comment_counnt, int pos) {

        ImagesCommentF comment_f = new ImagesCommentF(Integer.parseInt(comment_counnt));
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.in_from_bottom, R.anim.out_to_top, R.anim.in_from_top, R.anim.out_from_bottom);
        Bundle args = new Bundle();
        args.putString("video_id", "1");
        args.putString("user_id", "1");
        args.putInt("position", pos);
        Utilities.setPictureComments(context, pictureDataModels.get(pos).getComments());
        args.putSerializable("data", "item");
        comment_f.setArguments(args);
        transaction.addToBackStack(null);
        transaction.replace(R.id.container, comment_f).commit();

    }
}

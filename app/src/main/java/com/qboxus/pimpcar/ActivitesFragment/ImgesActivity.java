package com.qboxus.pimpcar.ActivitesFragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.database.DatabaseReference;
import com.ironsource.mediationsdk.IronSource;
import com.qboxus.pimpcar.ActivitesFragment.VideoRecording.VideoRecoderA;
import com.qboxus.pimpcar.ActivitesFragment.imagesPart.ImagesCommentF;
import com.qboxus.pimpcar.Adapters.ImagePostAdapter;
import com.qboxus.pimpcar.ApiClasses.Interfaces.FragmentDataSend;
import com.qboxus.pimpcar.Constants;
import com.qboxus.pimpcar.Models.HomeModel;
import com.qboxus.pimpcar.R;
import com.qboxus.pimpcar.SimpleClasses.Functions;
import com.qboxus.pimpcar.SimpleClasses.PermissionUtils;
import com.qboxus.pimpcar.SimpleClasses.Variables;
import com.qboxus.pimpcar.retrofitServices.GetDateService;
import com.qboxus.pimpcar.retrofitServices.RetrofitClientInstance;
import com.qboxus.pimpcar.retrofitServices.models.GetAllPicResponseModel;
import com.qboxus.pimpcar.retrofitServices.models.PictureDataModel;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ImgesActivity extends AppCompatActivity implements View.OnClickListener, ImagePostAdapter.Callback {

    Context context;
    RecyclerView recyclerView;
    ImageView btnBack, pimpModeButton, uploadImage;
    DatabaseReference rootref;
    String Imagetype = "";
    private LinearLayoutManager manager;
    private List<PictureDataModel> pictureDataModels;
    private String mCurrentPhotoPath;
    private static final int PICK_PHOTO = 12345;
    PermissionUtils takePermissionUtils;
    private static final int CAMERA_PIC_REQUEST = 54321;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Functions.setLocale(Functions.getSharedPreference(ImgesActivity.this).getString(Variables.APP_LANGUAGE_CODE, Variables.DEFAULT_LANGUAGE_CODE)
                , this, ImgesActivity.class, false);
        setContentView(R.layout.fragment_all_post);
        context = ImgesActivity.this;
        IronSource.init(this, "11e2d1a55", IronSource.AD_UNIT.BANNER);
        IronSource.init(this, "11e2d1a55", IronSource.AD_UNIT.INTERSTITIAL);
        if (!Constants.IS_REMOVE_ADS)
            loadAdd();


//        takePermissionUtils = new PermissionUtils(ImgesActivity.this, mPermissionResult);
        btnBack = findViewById(R.id.back_btn);
        pimpModeButton = findViewById(R.id.pimpModeButton);
        uploadImage = findViewById(R.id.uploadImage);
        btnBack.setOnClickListener(this);
        uploadImage.setOnClickListener(this);
        pimpModeButton.setOnClickListener(this);
        recyclerView = (RecyclerView) findViewById(R.id.recylerview);
        getAllPicsApi();
    }

    @Override
    public void onDestroy() {
//        mPermissionResult.unregister();

        super.onDestroy();

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_btn:
                ImgesActivity.super.onBackPressed();
                break;
            case R.id.pimpModeButton:
                showAdd();
                IronSource.showRewardedVideo("DefaultRewardedVideo");
                showDialog(this);

                break;
            case R.id.uploadImage:
                showAdd();
                IronSource.showInterstitial("DefaultInterstitial");
                showDialog(this);

                break;
            default: {
            }
        }
    }

    public void showDialog(Activity activity) {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(activity);
        bottomSheetDialog.setContentView(R.layout.bottom_sheet_home);
        CircleImageView pimpmode = bottomSheetDialog.findViewById(R.id.circleImageView_pimpmode);
        CircleImageView upload = bottomSheetDialog.findViewById(R.id.circleImageView_upload);

        pimpmode.setOnClickListener(v -> {
            bottomSheetDialog.dismiss();
            showDialodPimpMode(activity, "pimpmode");
        });

        upload.setOnClickListener(v -> {
            bottomSheetDialog.dismiss();
            showDialodPimpMode(activity, "upload");
        });

        bottomSheetDialog.show();

    }

    private void showDialodPimpMode(Activity activity, String str) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.select_pic_dialog_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        RelativeLayout gallery_button = dialog.findViewById(R.id.gallery_button);
        RelativeLayout camera_button = dialog.findViewById(R.id.camera_button);
        RelativeLayout video_button = dialog.findViewById(R.id.video_button);

        Imagetype = str;
        gallery_button.setOnClickListener(v -> {
            openGalleryWindow();
            dialog.dismiss();

        });

        camera_button.setOnClickListener(v -> {
            takePhotoFromCamera();
            dialog.dismiss();
        });

        video_button.setOnClickListener(v -> {
            dialog.dismiss();
            if (str.equals("pimpmode")) {
//            video_button.setVisibility(View.GONE);
//                openGalleryWindow();
                openGalleryForVideos();
            } else {
                openGalleryForVideos();

            }

        });
        dialog.show();
    }

    public void takePhotoFromCamera() {
        try {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (cameraIntent.resolveActivity(getPackageManager()) != null) {
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException ex) {
                }
                if (photoFile != null) {

                    Uri photoURI = FileProvider.getUriForFile(this,
                            "com.pimpcar.provider",
                            photoFile);

                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);


                    if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
                        cameraIntent.setClipData(ClipData.newRawUri("", photoURI));
                        cameraIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    }

                    startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private File createImageFile() throws IOException {
// Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";

        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName, // prefix
                ".jpg", // suffix
                storageDir // directory
        );

// Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void openGalleryWindow() {
        if (Build.VERSION.SDK_INT < 19) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/* video/*");
            startActivityForResult(intent, PICK_PHOTO);
        } else {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_MIME_TYPES, new String[]{"image/*", "video/*"});
            startActivityForResult(intent, PICK_PHOTO);
        }
    }

    private void openGalleryForVideos() {
//        if (Build.VERSION.SDK_INT < 19) {
//            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//            intent.setType("video/*");
//            startActivityForResult(intent, PICK_PHOTO);
//        } else {
//            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
//            intent.addCategory(Intent.CATEGORY_OPENABLE);
//            intent.setType("video/*");
//            intent.putExtra(Intent.EXTRA_MIME_TYPES, new String[]{"video/*"});
//            startActivityForResult(intent, PICK_PHOTO);
//        }
        Intent intent = new Intent(this, VideoRecoderA.class);
        startActivity(intent);
        overridePendingTransition(R.anim.in_from_bottom, R.anim.out_to_top);
    }
    protected void onResume() {
        super.onResume();
        IronSource.onResume(this);
    }
    protected void onPause() {
        super.onPause();
        IronSource.onPause(this);
    }
    InterstitialAd mInterstitialAd;
    public void loadAdd() {

        mInterstitialAd = new InterstitialAd(context);
        mInterstitialAd.setAdUnitId(context.getResources().getString(R.string.my_Interstitial_Add));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }
        });


    }


    public void showAdd() {
        if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }

    private void getAllPicsApi() {

        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
        Call<GetAllPicResponseModel> call = service.getAllPics("1");
        call.enqueue(new Callback<GetAllPicResponseModel>() {
            @Override
            public void onResponse(Call<GetAllPicResponseModel> call, Response<GetAllPicResponseModel> response) {

                pictureDataModels = response.body().getPosts().getData();
                if (pictureDataModels != null && !pictureDataModels.isEmpty()) {

                    showcurrentOrders(pictureDataModels, recyclerView);

                } else {
                    Toast.makeText(ImgesActivity.this, "No data found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetAllPicResponseModel> call, Throwable t) {
                Toast.makeText(ImgesActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });

    }

    private void showcurrentOrders(List<PictureDataModel> list, RecyclerView recyclerView) {
        recyclerView.setHasFixedSize(true);
        ImagePostAdapter adapter = new ImagePostAdapter(list, ImgesActivity.this, ImgesActivity.this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(ImgesActivity.this));
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void onItemClick(int pos) {

    }
//    private void openComment(int comment_counnt) {
//
//        ImagesCommentF comment_f = new ImagesCommentF(comment_counnt);
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.setCustomAnimations(R.anim.in_from_bottom, R.anim.out_to_top, R.anim.in_from_top, R.anim.out_from_bottom);
//        Bundle args = new Bundle();
//        args.putString("video_id", item.video_id);
//        args.putString("user_id", item.user_id);
//        args.putSerializable("data", item);
//        comment_f.setArguments(args);
//        transaction.addToBackStack(null);
//        transaction.replace(fragmentContainerId, comment_f).commit();
//
//    }
}

package com.qboxus.pimpcar.ActivitesFragment.imagesPart.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.gson.JsonObject;
import com.like.LikeButton;
import com.qboxus.pimpcar.ActivitesFragment.imagesPart.dialog.ActionBottomDialogFragment;
import com.qboxus.pimpcar.R;
import com.qboxus.pimpcar.retrofitServices.models.PictureCommentsModel;
import com.qboxus.pimpcar.utils.Utilities;
import com.qboxus.pimpcar.wiggets.progressbar.KProgressHUD;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ParentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<PictureCommentsModel> parentChildData;
    Context ctx;
    BottomSheetDialogFragment fragment = null;
    KProgressHUD hud;
    Callback callback;
    String commentId = "", user_token = "";

    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

    BottomSheetDialog bottomSheetDialog;
//    List<CommentsLikesModel> commentsLikesModels;
    String sizeOfLikes;

    public ParentAdapter(ActionBottomDialogFragment dialog) {
        this.fragment = dialog;
    }

    public ParentAdapter(Context ctx, List<PictureCommentsModel> parentChildData, Callback callback) {
        this.ctx = ctx;
        this.parentChildData = parentChildData;
        this.callback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment_layout, parent, false);
        ViewHolder pavh = new ViewHolder(itemLayoutView);
        return pavh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder vh = (ViewHolder) holder;
        PictureCommentsModel p = parentChildData.get(position);
        vh.bind(position);
//        initChildLayoutManager(vh.rv_child, p.getComment_replies());
        String timeStamp = String.valueOf(Calendar.getInstance().getTimeInMillis());
        String postunderScoreId = Utilities.getString(ctx, "postunderScoreId");
        user_token = Utilities.getString(ctx, "user_token");
        vh.message.setText(p.getBody());
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM,yyyy");
//        String dateString = formatter.format(new Date(Long.parseLong(p.getCreated_at())));
//        String MyFinalValue = getlongtoago(Long.parseLong(p.getCreated_at()));
//        vh.tvDate.setText(MyFinalValue);

        vh.username.setText(p.getUser().getName().toLowerCase());
//        sizeOfLikes = String.valueOf(p.get);
//        Toast.makeText(ctx, "" + sizeOfLikes, Toast.LENGTH_SHORT).show();
//        vh.likeTxt.setText(sizeOfLikes);
//        Picasso.with(ctx).load(p.getProfile_pic()).placeholder(R.drawable.profile_icon).into(vh.profile_image);
//        commentsLikesModels = new ArrayList<>();
//        getCommentsApi(videoItem.getUnderScoreId());
//        commentsLikesModels = p.getComment_likes();

//        if (commentsLikesModels.size() == 0) {
//            vh.post_like_button.setImageResource(R.drawable.ic_baseline_favorite_border_24);
//
//
//        }
//        for (int i = 0; i <= commentsLikesModels.size() - 1; i++) {
//
//            if (commentsLikesModels.size() > 0) {
//                String underScoreIdd = p.getComment_likes().get(i).getUser_id();
//                if (underScoreIdd.equals(HelperMethods.get_user_id())) {
//                    vh.post_like_button.setImageResource(R.drawable.ic_baseline_favorite_24);
//                } else {
//                    vh.post_like_button.setImageResource(R.drawable.ic_baseline_favorite_border_24);
//
//                }
//            }


//        }
//        commentsLikesModels = new ArrayList<>();
////        getCommentsApi(videoItem.getUnderScoreId());
//        for (int j = 0; j <= parentChildData.size() - 1; j++) {
//            commentsLikesModels = parentChildData.get(j).getComment_likes();
//
//            if (commentsLikesModels.size() == 0) {
//                post_like_button.setVisibility(View.VISIBLE);
//                postUnLike.setVisibility(View.GONE);
//            }
//            for (int i = 0; i <= commentsLikesModels.size() - 1; i++) {
//                if (commentsLikesModels.size() > 0) {
//                    String underScoreIdd = commentsLikesModels.get(i).getUser_id();
//                    if (underScoreIdd.equals(HelperMethods.get_user_id())) {
//                        post_like_button.setVisibility(View.GONE);
//                        postUnLike.setVisibility(View.VISIBLE);
//                    } else {
//                        post_like_button.setVisibility(View.VISIBLE);
//                        postUnLike.setVisibility(View.GONE);
//
//                    }
//                }
//
//
//            }
//
//        }

//        if (HelperMethods.get_user_id().equals(p.getUser_id())) {
//            vh.deleteComment.setVisibility(View.VISIBLE);
//
//        }
//        vh.itemView.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//                if (HelperMethods.get_user_id().equals(p.getUser_id())) {
////                    vh.deleteComment.setVisibility(View.VISIBLE);
//                    showCustomDialogForDeleteComment(p.get_id(), vh.getAdapterPosition());
//                } else {
//                    Toast.makeText(ctx, "you can delete your own comment ", Toast.LENGTH_SHORT).show();
//                }
//                return true;
//            }
//        });
//        vh.deleteComment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//
//            }
//        });
//        vh.profile_image.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent i = new Intent(ctx, OtherUserProfileActivity.class);
//                i.putExtra(Constants.THIRD_USER_ID, p.getUser_id());
//                ctx.startActivity(i);
//            }
//        });
//        vh.post_like_button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (vh.post_like_button.getDrawable().getConstantState() == ctx.getResources().getDrawable(R.drawable.ic_baseline_favorite_border_24).getConstantState()) {
////                    Toast.makeText(ctx, "dolike", Toast.LENGTH_SHORT).show();
//                    commentsLikeApi(p.get_id(), vh.post_like_button, postunderScoreId, HelperMethods.get_user_id(), timeStamp, p.getComment_likes().size(), "dolike", vh.totalLikes);
//
//
//                } else {
////                    Toast.makeText(ctx, "unlike", Toast.LENGTH_SHORT).show();
//
//                    commentsLikeApi(p.get_id(), vh.post_like_button, postunderScoreId, HelperMethods.get_user_id(), timeStamp, p.getComment_likes().size(), "unlike", vh.totalLikes);
//
//                }
//
//
//            }
//        });

//        vh.tvReply.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                bottomSheetDialog = new BottomSheetDialog(ctx);
//                bottomSheetDialog.setContentView(R.layout.create_comment_dialog);
//                ((Activity) ctx).getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//
//                final RelativeLayout rlSend = bottomSheetDialog.findViewById(R.id.rlSend);
//                final EditText etComments = bottomSheetDialog.findViewById(R.id.etComments);
//
//                rlSend.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        String posUunderScoreid = Utilities.getString(ctx, "postunderScoreId");
//                        String commentText = etComments.getText().toString();
//                        if (!commentText.equals("")) {
//                            createCommentApi(commentText, p.get_id(), posUunderScoreid, HelperMethods.get_user_id(), timeStamp);
//                            bottomSheetDialog.cancel();
//
//                        } else {
//                            Toast.makeText(ctx, "write Something", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                });
//
//
//                bottomSheetDialog.show();
//            }
//        });

    }

//    private void initChildLayoutManager(RecyclerView rv_child, List<CommentsReplyTextModel> childData) {
//
//        rv_child.setLayoutManager(new NestedRecyclerLinearLayoutManager(ctx));
//        ChildAdapter childAdapter = new ChildAdapter(ctx, childData);
//        rv_child.setAdapter(childAdapter);
//    }

    @Override
    public int getItemCount() {
        return parentChildData.size();
    }


//    private void commentsLikeApi(String commentid, AppCompatImageView postLikeBtn, String postId, String userid, String timestamp, int totalLieks, String status, TextView totalLikes) {
//        hud = KProgressHUD.create(ctx)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
//        hud.setSize(50, 50);
//        hud.show();
//        JsonObject jsonObject = new JsonObject();
//        JsonObject attributes = new JsonObject();
//        jsonObject.addProperty("comment_id", commentid);
//        jsonObject.add("comment_likes", attributes);
//        attributes.addProperty("user_id", userid);
//        attributes.addProperty("timestamp", timestamp);
//
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(VideoInterface.VIDEOURL)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//
//        //Create a file object using file path
//
//        VideoInterface getResponse = retrofit.create(VideoInterface.class);
////        Call<String> call = getResponse.uploadImage(fileToUpload,"descriptions","hashtags","userTags","6184be3e6274da6a8865e3ed","1637392740");
//        Call<CommentsLikeResponseModel> call = getResponse.getCommetnsLikeApi(postId, jsonObject);
//        Log.d("assss", "asss");
//        call.enqueue(new retrofit2.Callback<CommentsLikeResponseModel>() {
//            @Override
//            public void onResponse(Call<CommentsLikeResponseModel> call, Response<CommentsLikeResponseModel> response) {
//                if (response.code() == 200) {
//                    hud.dismiss();
//                    if (response.body().getMessage().equals("comment liked")) {
//                        int totallikesIncremnt = Integer.parseInt(totalLikes.getText().toString()) + 1;
//                        totalLikes.setText(String.valueOf(totallikesIncremnt));
//                        postLikeBtn.setImageResource(R.drawable.ic_baseline_favorite_24);
//
//                        Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                    } else {
//                        if (totalLikes.getText().toString().equals("1")) {
//                            totalLikes.setText("0");
////                            Toast.makeText(ctx, "call", Toast.LENGTH_SHORT).show();
//                            postLikeBtn.setImageResource(R.drawable.ic_baseline_favorite_border_24);
//                            Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_SHORT).show();
//
//                        } else {
//                            int totallikesIncremnt = Integer.parseInt(totalLikes.getText().toString()) - 1;
//                            totalLikes.setText(String.valueOf(totallikesIncremnt));
//                            postLikeBtn.setImageResource(R.drawable.ic_baseline_favorite_border_24);
//                            Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_SHORT).show();
//
//                        }
//
//
//                    }
//
//                } else {
//                    hud.dismiss();
//
//
//                    Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_SHORT).show();
//
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call call, Throwable t) {
//                hud.dismiss();
//                Log.d("gttt", call.toString());
//                Toast.makeText(ctx, "failed", Toast.LENGTH_SHORT).show();
//
//            }
//        });
//
//    }
    //fdfd

    public interface Callback {
        void onItemClick(int pos);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView username, tvReply, message, replyCount, likeTxt, showLessTxt;
        SimpleDraweeView userPic;
        ImageView likeImage;
        LinearLayout messageLayout, lessLayout, likeLayout;
        RecyclerView replyRecyclerView;


        public ViewHolder(@NonNull View view) {
            super(view);

            username = view.findViewById(R.id.username);
            userPic = view.findViewById(R.id.user_pic);
            message = view.findViewById(R.id.message);
            replyCount = view.findViewById(R.id.reply_count);
            likeImage = view.findViewById(R.id.like_image);
            messageLayout = view.findViewById(R.id.message_layout);
            tvReply = view.findViewById(R.id.tvReply);
            likeTxt = view.findViewById(R.id.like_txt);
            replyRecyclerView = view.findViewById(R.id.reply_recycler_view);
            lessLayout = view.findViewById(R.id.less_layout);
            showLessTxt = view.findViewById(R.id.show_less_txt);
            likeLayout = view.findViewById(R.id.like_layout);
        }

        private void bind(int pos) {
            PictureCommentsModel messagesTabModel = parentChildData.get(pos);
            initClickListener();
        }

        private void initClickListener() {
            tvReply.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.onItemClick(getAdapterPosition());
                }
            });
        }
    }


    public static String getlongtoago(long createdAt) {
        DateFormat userDateFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
        DateFormat dateFormatNeeded = new SimpleDateFormat("MM/dd/yyyy HH:MM:SS");
        Date date = null;
        date = new Date(createdAt);
        String crdate1 = dateFormatNeeded.format(date);

        // Date Calculation
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        crdate1 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(date);

        // get current date time with Calendar()
        Calendar cal = Calendar.getInstance();
        String currenttime = dateFormat.format(cal.getTime());

        Date CreatedAt = null;
        Date current = null;
        try {
            CreatedAt = dateFormat.parse(crdate1);
            current = dateFormat.parse(currenttime);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Get msec from each, and subtract.
        long diff = current.getTime() - CreatedAt.getTime();
        long diffSeconds = diff / 1000;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);

        String time = null;
        if (diffDays > 0) {
            if (diffDays == 1) {
                time = diffDays + " day ago ";
            } else {
                time = diffDays + " days ago ";
            }
        } else {
            if (diffHours > 0) {
                if (diffHours == 1) {
                    time = diffHours + " hr ago";
                } else {
                    time = diffHours + " hrs ago";
                }
            } else {
                if (diffMinutes > 0) {
                    if (diffMinutes == 1) {
                        time = diffMinutes + " min ago";
                    } else {
                        time = diffMinutes + " mins ago";
                    }
                } else {
                    if (diffSeconds > 0) {
                        time = diffSeconds + " secs ago";
                    }
                }

            }

        }
        return time;
    }

//    private void deleteCommentsApi(String commentId, int position) {
//        hud = KProgressHUD.create(ctx)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
//        hud.setSize(50, 50);
//        hud.show();
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(VideoInterface.VIDEOURL)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//
//        //Create a file object using file path
//
//        VideoInterface getResponse = retrofit.create(VideoInterface.class);
//        Call<CommentsLikeResponseModel> call = getResponse.delete_commentApi(commentId);
//        call.enqueue(new retrofit2.Callback<CommentsLikeResponseModel>() {
//            @Override
//            public void onResponse(Call<CommentsLikeResponseModel> call, Response<CommentsLikeResponseModel> response) {
//                if (response.code() == 200) {
//                    hud.dismiss();
////                    PictureCommentsModels.add(new PictureCommentsModel(HelperMethods.get_user_id(),commentText,timestamp));
////                    parentAdapter.notifyDataSetChanged();
////                    dismiss();
//                    parentChildData.remove(position);
//                    notifyItemRemoved(position);
//                    notifyItemRangeChanged(position, parentChildData.size());
//                    Toast.makeText(ctx, "comment Deleted Successfully", Toast.LENGTH_SHORT).show();
//
//
//                } else {
//                    hud.dismiss();
//                    Toast.makeText(ctx, "failed to delete", Toast.LENGTH_SHORT).show();
//
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call call, Throwable t) {
//                hud.dismiss();
//                Log.d("gttt", call.toString());
//                Toast.makeText(ctx, "failed", Toast.LENGTH_SHORT).show();
//
//            }
//        });
//
//    }
//
//    private void showCustomDialogForDeleteComment(String commentId, int pos) {
//        final PrettyDialog pDialog = new PrettyDialog(ctx);
//        pDialog
//                .setTitle("Message")
//                .setMessage("Are you sure you want to Delete Comment?")
//                .setIcon(R.drawable.pdlg_icon_info)
//                .setIconTint(R.color.colorPrimary)
//                .addButton(
//                        "Yes",
//                        R.color.pdlg_color_white,
//                        R.color.colorPrimary,
//                        new PrettyDialogCallback() {
//                            @Override
//                            public void onClick() {
//                                deleteCommentsApi(commentId, pos);
//                                pDialog.dismiss();
//                            }
//                        }
//                )
//                .addButton("No",
//                        R.color.pdlg_color_white,
//                        R.color.pdlg_color_red,
//                        new PrettyDialogCallback() {
//                            @Override
//                            public void onClick() {
//                                pDialog.dismiss();
//                            }
//                        })
//                .show();
//
//    }
}
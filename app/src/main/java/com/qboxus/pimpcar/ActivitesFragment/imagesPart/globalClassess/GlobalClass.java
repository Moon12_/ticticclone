package com.qboxus.pimpcar.ActivitesFragment.imagesPart.globalClassess;

import android.app.Application;

import com.qboxus.pimpcar.retrofitServices.models.PictureCommentsModel;

import java.util.List;

public class GlobalClass extends Application {
    private List<PictureCommentsModel> pictureCommentsModels;

    public List<PictureCommentsModel> getPictureCommentsModels() {
        return pictureCommentsModels;
    }

    public void setPictureCommentsModels(List<PictureCommentsModel> pictureCommentsModels) {
        this.pictureCommentsModels = pictureCommentsModels;
    }
}

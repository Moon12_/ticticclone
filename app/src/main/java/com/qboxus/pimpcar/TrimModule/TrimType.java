package com.qboxus.pimpcar.TrimModule;

public enum TrimType {
    DEFAULT, FIXED_DURATION, MIN_DURATION, MIN_MAX_DURATION
}
